/*==================[inclusions]=============================================*/

#include "main.h"
#include "memoria.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t tx_buf[] = {0xaa, 0xbb, 0xcc};
    uint8_t rx_buf[3];

    memWrite(MEM_SLAVE_ADDR, 0, tx_buf, sizeof(tx_buf));

    memRead(MEM_SLAVE_ADDR, 0, rx_buf, sizeof(rx_buf));

    return 0;
}

/*==================[end of file]============================================*/
