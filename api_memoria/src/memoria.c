/*==================[inclusions]=============================================*/

#include "memoria.h"
#include <string.h>
#include <stdio.h>

/*==================[macros and definitions]=================================*/

#define MAX_XFER_SIZE  8
#define MEM_SIZE       32

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static uint8_t memory[MEM_SIZE];

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void memTransfer(MEM_XFER_T *xfer)
{
    if ((xfer->rxSz > MAX_XFER_SIZE || xfer->txSz > MAX_XFER_SIZE) ||
        (xfer->rxSz == 0 && xfer->txSz == 0) ||
        (xfer->addr + xfer->txSz + xfer->rxSz) > MEM_SIZE) {
        xfer->status = -1;
    }
    else if (xfer->rxSz > 0) {
        printf("Leyendo datos de la memoria \n");
        for (uint16_t i = 0; i < xfer->rxSz; i++) {
            xfer->rxBuff[i] = memory[i + xfer->addr];
        }
    }
    else if (xfer->txSz > 0) {
        printf("Escribiendo datos a la memoria \n");
        for (uint16_t i = 0; i < xfer->txSz; i++) {
            memory[i + xfer->addr] = xfer->txBuff[i];
        }

        printf("Estado actual de la memoria: \n");
        for (uint16_t i = 0; i < MEM_SIZE; i++) {
            printf("0x%.2x ", memory[i]);
        }
        printf("\n\n");
    }
    xfer->status = 0;
}

/*==================[external functions definition]==========================*/

int16_t memWrite(uint8_t id, uint16_t addr, void *data, uint16_t len)
{
    /* ... */

    return 0;
}

int16_t memRead(uint8_t id, uint16_t addr, void *data, uint16_t len)
{
    /* ... */

    return 0;
}

/*==================[end of file]============================================*/
